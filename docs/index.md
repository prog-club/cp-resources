# CP Resources

Welcome to CP Resources. Here you will find:

1. answers to commonly asked questions. What editor to use for CP?
2. resources for practice. Where to practice dp from?
3. useful tips and tricks for your CP routine. Common mistakes that cause TLE.

Use the links in the sidebar to start exploring! Or search using the top sidebar!
