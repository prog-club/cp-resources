Welcome to CP! You have chosen a path that is of great interest to everyone.

Getting started is not tricky at all. We recommend the book by Artti Laaksonen called [Competitive Programmer's handbook](https://cses.fi/book/index.php). Through that book, you will be solving problems from the [CSES problemset](https://cses.fi/problemset/), which is an excellent resource for beginners, as it contains several key problems from each major category.

Alongside CSES, you'll also want to keep solving more problems, since practice makes perfect. Use [Codeforces](https://codeforces.com/problemset) to find more problems to practice. In general, search for problems with the same tags as that topic which you did on CSES. Sort them by their rating, and move up rating as and when you feel confident.

You can also use [A2OJ category lists](https://www.a2oj.com/Categories.html) to practice.
