Before asking others for help, make sure you have put in your best effort in solving the problem. Most people would be happy to help you, but only when you put in your own honest effort into it first.

1. Ask other people who you can contact in person and who have solved the same problem. You can find their submissions in the standings page.
2. Messaging random people on CF or Facebook, is generally a bad idea.
3. Post your problem in the editorial section of a blog post.

