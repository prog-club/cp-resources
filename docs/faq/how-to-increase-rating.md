Don't worry too much about rating. Rating should be a consequence, not a cause. Don't get disheartened by a few bad performances. Keep your focus on learning and solving.

To increase your performance in CP, check out the linked guide.
