Getting stuck happens to everyone! But how you manage when stuck defines how fast you learn. If you end up being stuck a lot of times, you are probably doing something wrong and need to revisit how to approach a problem.

Read this [blog post](https://kauntaofficial.github.io/2020/05/17/the-art-of-thinking.html) to understand how to work your way through a problem when stuck. In summary, the blog stresses three key steps:

1. Writing down all known variables
2. Writing down the immediate next step available to you
3. Writing down all simplifications available after taking the next step, and repeating step 2 until problem is solved.

In general, this relies on the fact that since the number of possible techniques being used in a problem are quite limited, the above will eventually converge to the correct answer. This does not work, of course, when the problem requires an entirely new data structure or topic.
