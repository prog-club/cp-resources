# CP Resources

CP resources a **reference guide** and an **FAQ helper** for competitive coders at all levels.

Here you will find:

- answers to commonly asked questions. What editor to use for CP?
- resources for practice. Where to practice dp from?
- useful tips and tricks for your CP routine. Common mistakes that cause TLE.


## Contribute

This is an open source project, all PRs are welcome.